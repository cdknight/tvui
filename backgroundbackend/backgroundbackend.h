#ifndef BACKGROUNDBACKEND_H
#define BACKGROUNDBACKEND_H

#include <QObject>
#include <QString>
#include <QNetworkReply>

class BackgroundBackend: public QObject
{
    Q_OBJECT
    Q_PROPERTY (QString wallpaperUrl READ wallpaperUrl NOTIFY wallpaperChanged); // These properties get used by QML
    Q_PROPERTY (QString description READ description NOTIFY descriptionChanged);

public:
    explicit BackgroundBackend(QObject* parent = nullptr);
    QString wallpaperUrl(); // Getters
    QString description();
signals:
    void wallpaperChanged();
    void descriptionChanged();
public slots:
    void handleWallpaperRequest(QNetworkReply *imageDataResponse);
private:
    QString wallpaper_url;
    QString description_str;
    void initiateWallpaperRequest();
};

#endif // BACKGROUNDBACKEND_H
