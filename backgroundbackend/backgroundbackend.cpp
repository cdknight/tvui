#include "backgroundbackend.h"
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QJsonObject>
#include <QJsonDocument>
#include <QString>
#include <QObject>
#include <QJsonValueRef>
#include <QJsonArray>
#include <QDebug>

BackgroundBackend::BackgroundBackend(QObject* parent): QObject(parent)
{
    this->wallpaper_url = QString(); // Initialize stuff.
    this->description_str = QString();
}
void BackgroundBackend::handleWallpaperRequest(QNetworkReply* imageDataResponse) {
    QJsonDocument imageDataJson = QJsonDocument::fromJson(imageDataResponse->readAll()); // We are handling the wallpaper request from Bing, who responds with JSON. So we turn the response to JSON.
    QJsonObject imageDataJo = imageDataJson.object(); // Kind-of like "1-layer down from QJsonDocument." We can actually use this.

    QJsonObject spImageJo = imageDataJo["images"].toArray()[0].toObject(); // More JSON parsing. If you really want more info, you can do the request yourself and see how this matches to that.
    this->wallpaper_url = QString("https://bing.com") + spImageJo["url"].toString(); // Set stuff from JSON
    this->description_str = spImageJo["title"].toString() + ": " + spImageJo["copyright"].toString();
    emit wallpaperChanged(); // Tell the UI that we finished updating stuff so it can update.
    emit descriptionChanged();
}


// Just a getter. If the wallpaper is blank, though, we make Bing fetch a new wallpaper.
QString BackgroundBackend::wallpaperUrl() {
    if (this->wallpaper_url.isEmpty()) {
        this->initiateWallpaperRequest();
    }
    return this->wallpaper_url;
}

// Another getter. Nothing interesting here.
QString BackgroundBackend::description() {
    return this->description_str;
}

void BackgroundBackend::initiateWallpaperRequest()  {
    QNetworkAccessManager* manager = new QNetworkAccessManager(this); // Be able to do requests
    QNetworkRequest request;
    request.setUrl(QUrl("https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US")); // Set request URL. Nothing fancy here. I got the URL from stackoverflow.

    QObject::connect(manager, &QNetworkAccessManager::finished, this, &BackgroundBackend::handleWallpaperRequest); // Handle the response with a member function (handleWallpaperRequest)
    manager->get(request); // This initiates the request.
}
