#ifndef ALARMCONTROLLER_H
#define ALARMCONTROLLER_H

#include <QObject>

class AlarmController: public QObject // This is just a class definition
{
    Q_OBJECT
    Q_PROPERTY(QString alarmTime READ alarmTime WRITE setAlarmTime NOTIFY alarmTimeChanged) // These properties get used by QML
    Q_PROPERTY(QString alarmFile READ alarmFile NOTIFY alarmFileChanged)

public:
    explicit AlarmController(QObject* object = nullptr);
    QString alarmTime(); // getters
    QString alarmFile();
public slots:
    void setAlarmTime(QString newTime); // Setter
signals:
    void alarmTimeChanged();
    void alarmFileChanged();
private:
    QString alarm_time;
    QString alarm_file;

};

#endif // ALARMCONTROLLER_H
