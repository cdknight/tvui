#include "alarmcontroller.h"
#include <QObject>
#include <QSettings>
#include <QDateTime>
#include <QDebug>
#include <QTimer>

AlarmController::AlarmController(QObject *parent): QObject(parent)
{

    QSettings* alarmSettings = new QSettings("tvrui", "alarmSettings"); // TODO make this destroyed in the destructor
    QDateTime alarmDateTime = QDateTime::fromString(alarmSettings->value("alarmTime").toString(), "MM.dd.yyyy hh:mm ap"); // Something like 06.12.2020 12:30 am. Make sure you have the zeroes in your time, like 03:08 pm (otherwise it won't work).

    this->alarm_file = alarmSettings->value("alarmFile").toString(); // This is the absolute path to the alarm file.
    emit alarmFileChanged(); // Tell the UI to update.

    qDebug() << "(debug alarm) User-specified QDateTime is" << alarmDateTime;

    this->alarm_time = QString(""); // Initialize this to be blank.
    emit alarmTimeChanged();

    qDebug() << "(debug alarm)" << QDateTime::currentDateTime().secsTo(alarmDateTime) << "seconds to the alarm."; // Basically we get the number of seconds between now and the alarm.

    // By getting the # of secs between now and the alarm, we set up two timers (which will just call a function in a certain number of ms), one to begin and one to end the alarm.
    // Since the timers are specified in ms we multiply by 1000.

    QTimer::singleShot((QDateTime::currentDateTime().secsTo(alarmDateTime) * 1000) + 500, this, [this, alarmDateTime] { // Start the alarm. We add 500 ms because it seems to start a tad bit early.
        setAlarmTime(alarmDateTime.toString("hh:mm ap"));
    });

    QTimer::singleShot((QDateTime::currentDateTime().secsTo(alarmDateTime) * 1000) + (2*60*1000), this, [this, alarmDateTime] { // Alarm dismisses after two minutes. This does interfere with the dismiss button, since both set the value of alarm_time to "".
        setAlarmTime("");
    });
}

// Two getter methods.
QString AlarmController::alarmTime() {
   return this->alarm_time;
}

QString AlarmController::alarmFile() {
   return this->alarm_file;
}

// This is called by the timer. This updates the string for the alarm time which displays if there is an alarm.
// (We can set the alarmTime to "" which will "clear" the alarm).
void AlarmController::setAlarmTime(QString alarmTime) {
    qDebug() << "(debug alarm) Running" << alarmTime;
    this->alarm_time = alarmTime;

    emit alarmTimeChanged();
}

