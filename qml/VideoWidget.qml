import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtMultimedia 5.15
import cdknight.mediacontroller 1.0

Rectangle {
    width: Screen.width // Take up the entire screen.
    height: Screen.height
    visible: mediaController.videoFile !== "" // Visible if there is an actual video file set.
    color: "black"

    property alias mediaController: mediaController; // So we can access it easily.

    MediaController {
        objectName: "mediaController" // For the C++ control server to be able to access it.
        id: mediaController
        onVideoFileChanged: {
            console.log("mediacontroller QML, playing " + mediaController.videoFile)
        }
    }

    MediaPlayer {
        id: videoPlayer
        objectName: "videoPlayer" // For the C++ control server to be able to access it.
        source: "file://" + mediaController.videoFile
        autoPlay: true
    }


    VideoOutput {
        width: Screen.width // Full-screen video.
        height: Screen.height
        source: videoPlayer
    }

    Column{ // These are the video controls and the time.
        x: 5
        Button { // Play or Pause
            text: (videoPlayer.playbackState === MediaPlayer.PlayingState) ? "Pause" : "Play" // Choose the label based on whether the video is playing or not.
            onClicked: {
                if (videoPlayer.playbackState === MediaPlayer.PlayingState) { // If playing, pause, otherwise play
                    videoPlayer.pause();
                }
                else {
                    videoPlayer.play();
                }

            }
        }

        Button {
            text: "Stop"
            onClicked: {
                mediaController.videoFile = ""; // This will make the dl.py script fail, so in return the backend will set the videoFile to "" and we won't see the VideoWidget anymore.
            }

        }


        Text {
            color: "white"
            text: msToMinSec(videoPlayer.position) + "/" + msToMinSec(videoPlayer.duration) // We take the position/duration in ms and turn into minutes:seconds
            font.pixelSize: 24
            function msToMinSec(ms) {
                var mms = ms > 60000 ? ms % 60000 : ms; // This is ... like the number of seconds past the total number of minutes so far. Like for 1:30 it'd be 30.
                var mins = Math.floor(ms / 60000);  // The number of minutes passed so far.
                mins = mins.toString().length === 1 ? "0" + mins : mins // Add zero, like the number might be 3, but we want 03, so we do this.
                var secs = Math.floor(mms / 1000);
                secs = secs.toString().length === 1 ? "0" + secs : secs // Same thing as line for mins (add zero)
                return mins + ":" + secs;
            }
        }
    }


}
