import QtQuick 2.0
import QtQuick.Window 2.12

Rectangle { // Show this in the center of the screen while the video is loading.
    color: "white"

    width: 300
    height: 100

    anchors.centerIn: parent // Center (the parent should take up the entire screen, since I think it's either Window or Image)
    radius: 9 // Round the corners :)

    Text {
        anchors.centerIn: parent
        font.bold: true
        font.pixelSize: 32
        text: "Loading Video"
    }
}

