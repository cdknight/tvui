import QtQuick 2.0

Loader {
    property var dayData;
    source: (dayData["day"] === "Now") ? "WeatherInfoNow.qml" : "WeatherInfoDay.qml" // Conditionally load the weather display. If the weather is for "now" we want a different layout, so we choose a different file.
}
