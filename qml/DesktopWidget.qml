import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle { // For widgets
    property alias titleText: desktopWidgetTitle.text // Easy to specify a title
    property alias content: childrenRow.children // Children. So they are displayed.... in a COLUMN. I have to fix this later.
    color: "white"
    radius: 9 // Round the rectangle

    width: 500
    height: childrenRow.height + 60


    layer.enabled: true
    layer.effect: DropShadow { // Drop shadow makes things 1) look nice 2) blend together with the background
       color: "#333"
       radius: 10
       samples: 10
    }

    Column { // Children COLUMN
        x: 25 // Position it so it doesn't hit the edge of the rectangle
        y: 25
        id: childrenRow // column
        spacing: 5
        Text {
            id: desktopWidgetTitle // Title, as in at the top

            font.pixelSize: 35
            font.bold: true
        }

    }


}
