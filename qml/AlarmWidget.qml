import QtQuick 2.0
import QtQuick.Window 2.12
import QtQuick.Controls 2.15
import QtMultimedia 5.15
import cdknight.alarmcontroller 1.0

Rectangle {
    height: Screen.height
    width: (alarmController.alarmTime != "") ? (0.2) * Screen.width : 0 // A literal hack. Could probably use visible. But... The width is zero if the alarm is "" (in the cleared state).


    AlarmController {
        id: alarmController
        onAlarmTimeChanged: {
            if (alarmController.alarmTime !== "") {
                console.log("Playing alarm sound...")
                alarmSound.play(); // Play alarm sound only IF the alarm sound isn't blank.
            }
            else {
                console.log("Stopping alarm sound...")
                alarmSound.stop(); // If alarm sound is "" then we can clear it
            }

        }
    }

    MediaPlayer {
        id: alarmSound
        source: "file://" + alarmController.alarmFile
        loops: MediaPlayer.Infinite // Keep repeating until explicitly stopped

        onError: function (error, errorString) {
            console.log(error)
            console.log(errorString)
        }
    }

    Column {
        x: 25
        y: 25
        spacing: 20

        Text { // Title Alarm at top
            text: "Alarm"
            font.bold: true
            font.pointSize: 36
        }

        Text { // This is the alarm string (eg. 02:00 pm).
            text: alarmController.alarmTime

            font.bold: true
            font.pointSize: 24
        }

        Button {
            text: "Dismiss"
            onClicked: {
                alarmController.setAlarmTime(""); // Clear alarm by setting this to ""
            }
        }
    }
}
