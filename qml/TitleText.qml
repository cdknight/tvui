import QtQuick 2.0
import QtGraphicalEffects 1.0

Text {
    property string titleText: ""
    property int size: 48 // We can specify this manually. This is just the default value.

    text: titleText
    color: "white"

    font.bold: true
    font.pointSize: size

    layer.enabled: true
    layer.effect: DropShadow { // We add a drop shadow so there is contrast between the text and whatever's behind it.
        color: "#333"
        radius: 30
        samples: 30
    }

}

