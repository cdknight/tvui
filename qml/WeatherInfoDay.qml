import QtQuick 2.0

Column {
    property var unit: (dayData["units"] === "metric") ? "°C" : "°F" // Choose the appropriate unit so we can add it later.
    property var titleSize: 20 // Easily change the UI

    Text { font.pixelSize: 20; font.bold: true; text: dayData["day"] } // Day
    Row { // Below that, a row with the high that day and the description (eg. Clouds).
        spacing: 20
        Text { font.pixelSize: titleSize - 4; text: "High: " + dayData["high"] + unit }
        Text { font.bold: true; text: dayData["description"] }
    }

    Text { font.pixelSize: titleSize - 4; text: "Low: " + dayData["low"] + unit} // Show the low on the next line.
}
