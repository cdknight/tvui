import QtQuick 2.0
import cdknight.weatherprovider 1.0

DesktopWidget {

    id: weatherWidget
    titleText: "Weather"

    WeatherProvider { // Backend that does requests in C++
        id: weatherProvider
    }

    content: Repeater { // Repeat for each item in the weatherData list (we render a WeatherInfo component). Basically we render the entire list.
        model: weatherProvider.weatherData
        WeatherInfo {
            dayData: modelData
        }
    }



}
