import QtQuick 2.0



Column { // Individual display of weather for "now"
    property var unit: (dayData["units"] === "metric") ? "°C" : "°F" // Choose the right unit
    property var titleSize: 22;
    Row { // The day, temp, and description (eg. Clouds) in one line
        spacing: 20
        Text { font.pixelSize: titleSize; font.bold: true; text: dayData["day"] }
        Text { font.pixelSize: titleSize; text: dayData["now"] + unit }
        Text { font.pixelSize: titleSize; font.bold: true; text: dayData["description"] }
    }

    Text { font.pixelSize: titleSize - 5; text: "Feels Like: " + dayData["feelslike"] + unit} // Feels like.
}
