import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.0
import cdknight.backgroundbackend 1.0

Window { // Main UI
    id: page
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("Wall") // Title bar (which we can probably ignore eventually)

    BackgroundBackend {
        id: bgbackend // to get the background url.
    }



    Image { // The background image
        id: imageBackground
      source: bgbackend.wallpaperUrl // Load it from the backend which did the request to get the image url

      Column { // Children of the background image
          y: Screen.height - 175
          x: 25

         TitleText {

             id: titleTime
             text: new Date().toLocaleString(Qt.locale(), "hh:mm:ss A") // 02:03:47 AM/PM for example
             size: 48
             function updateTime() {
                 this.text = new Date().toLocaleString(Qt.locale(), "hh:mm:ss A");
             }

             Timer { // Make sure the time actually changes.
                 id: timeUpdateTimer
                 interval: 1000 // Every 1 second
                 repeat: true
                 running: true
                 triggeredOnStart: true
                 onTriggered: parent.updateTime() // Call updateTime so the time doesn't stay stagnant
             }
         }

         TitleText { // The thing at the bottom with the copyright and background description.
            text: bgbackend.description
            size: 12
         }




      }



    }

    Row { // Row of widgets

        x: 25 // spacing so it isn't right at the edge of the screen
        y: 50
        spacing: 50
        id: widgetrow

        WeatherWidget {
            id: weather // for the weather.
        }

        Row { // The little input box and button to load a video you see.
            TextField {
                placeholderText: "Input a Video URL..."
                id: videoInput
            }
            Button {
                id: videoSet
                text: "Play Video"
                onClicked: {
                    videoWidget.mediaController.videoFile = videoInput.text
                    videoInput.text = "";
                }
            }
        }

    }

    AlarmWidget { // This is usually hidden unless there's an alarm. It is "anchored" to the right of the screen.
        id: alarmWidget
        anchors.right: imageBackground.right
    }


    VideoWidget { // This is also hidden unless the video file isn't blank.
        id: videoWidget
        objectName: "videoWidget";
    }

    VideoLoadingWidget { // The box you see when you set the video URL.
        visible: videoWidget.mediaController.loading
    }




 }
