#include "weatherprovider.h"
#include <QObject>
#include <QSettings>
#include <QDebug>
#include <QList>
#include <QVariantMap>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

WeatherProvider::WeatherProvider(QObject* parent) : QObject(parent)
{
    // QSettings reads from the config file so we don't have to :)
    this->weatherSettings = new QSettings("tvrui", "weatherSettings"); // TODO make this destroyed in the destructor
    if (weatherSettings->value("lat").toDouble() == 0 || weatherSettings->value("long").toDouble() == 0) { // If the config file doesn't exist this will happnen (values will be 0)
        // Set some random values
        qDebug() << "(debug weather) Please update your weather location.";
        weatherSettings->setValue("lat", 1); // Set some default values for latlong if the file doesn't exit
        weatherSettings->setValue("long", 1);
    }

    if (weatherSettings->value("apikey").toString().isEmpty()) { // Set placeholders if the config file doesn't exist
        qDebug() << "(debug weather) Please set your api key.";
        weatherSettings->setValue("apikey", "ENTER YOUR API KEY");
    }

    if (weatherSettings->value("units").toString().isEmpty()) { // Set placeholders if the config file doesn't exist
        qDebug() << "(debug weather) Please specify if you want inmperial or metric units.";
        weatherSettings->setValue("units", "imperial/metric");
    }

    qDebug() << "(debug weather) Config file location" << weatherSettings->fileName();


    this->weather_data = QList<QVariantMap>(); // Initialize the weather_data QList so we can populate it with the weather data later.
}

void WeatherProvider::handleWeatherDataRequest(QNetworkReply* weatherDataResponse) {
    // Not fun to add comments on this one
    QJsonDocument weatherDataJson = QJsonDocument::fromJson(weatherDataResponse->readAll()); // Get the response as json
    QJsonArray weatherData = weatherDataJson.object()["daily"].toArray(); // Get an array for daily and current (1-week) weather data. We handle each separately
    QJsonObject weatherNow = weatherDataJson.object()["current"].toObject();

    this->weather_data.clear(); // Make sure we start from scratch.

    QVariantMap nowWeather; // List of QVariantMaps, so we make one for now and populate it with data from the JSON response (that's all that's happening below)
    nowWeather["day"] = QVariant("Now");
    nowWeather["now"] = QVariant(weatherNow["temp"]);
    nowWeather["feelslike"] = QVariant(weatherNow["feels_like"]);
    nowWeather["description"] = QVariant(weatherNow["weather"].toArray()[0].toObject()["main"]);
    nowWeather["units"] = QVariant(this->weatherSettings->value("units"));

    this->weather_data.append(nowWeather); // Add this to the weather data listj

    for (QJsonValueRef weatherInfoRef: weatherData) { // For each day
        QVariantMap dayWeather;
        QJsonObject weatherInfo = weatherInfoRef.toObject(); // Why can I do this

        // Populate temporary QVariantMap with data from the JSON response.

        dayWeather["high"] = QVariant(weatherInfo["temp"].toObject()["max"]);
        dayWeather["low"] = QVariant(weatherInfo["temp"].toObject()["max"]);
        dayWeather["description"] = QVariant(weatherInfo["weather"].toArray()[0].toObject()["main"]);

        QDateTime current = QDateTime::fromSecsSinceEpoch(weatherInfo["dt"].toInt());
        dayWeather["day"] = current.toString("dddd"); // Monday, Tuesday, etc. (that formatting based on the UNIX timestamp which is given from the API)
        dayWeather["units"] = QVariant(this->weatherSettings->value("units"));

        this->weather_data.append(dayWeather); // Add to the weather info list.
    }

    emit weatherDataChanged(); // Let everyone know the weather data is changed so the UI updates.
}

void WeatherProvider::initiateWeatherDataRequest() {
    QNetworkAccessManager* manager = new QNetworkAccessManager(this);
    QNetworkRequest request;
    // This might be a waste of memory (get stuff so we can make the request to the weather api easier to read/write)
    double lat = weatherSettings->value("lat").toDouble();
    double long_t = weatherSettings->value("long").toDouble();
    QString units = weatherSettings->value("units").toString();
    QString apikey = weatherSettings->value("apikey").toString();

    // The request itself. You can see it converts everything (parameter) to strings and joins it all together to get a long url that will be requested.
    QString reqUrl = QString("https://api.openweathermap.org/data/2.5/onecall?lat=") + QString::number(lat) +
                        "&lon=" + QString::number(long_t) + "&exclude=minutely,hourly&units=" + units + "&appid=" + apikey;
    qDebug() << "(debug weather) Requesting" << reqUrl;
    request.setUrl(QUrl(reqUrl)); // Set the request URL
    QObject::connect(manager, &QNetworkAccessManager::finished, this, &WeatherProvider::handleWeatherDataRequest); // We make it so that when the request is complete it calls handleWeatherDataRequest, which is one of the member methods.
    manager->get(request); // This starts the request
}

// Getter. If there is no data, it starts a request to populate the list (weather_data) with data.
QList<QVariantMap> WeatherProvider::weatherData() {
    if (this->weather_data.isEmpty()) {
        this->initiateWeatherDataRequest();
    }
    return this->weather_data;
}
