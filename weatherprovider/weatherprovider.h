#ifndef WEATHERPROVIDER_H
#define WEATHERPROVIDER_H

#include <QObject>
#include <QVariantMap>
#include <QString>
#include <QSettings>
#include <QList>
#include <QNetworkReply>

class WeatherProvider : public QObject
{
    Q_OBJECT
    Q_PROPERTY (QList<QVariantMap> weatherData READ weatherData NOTIFY weatherDataChanged); // A list of maps with weather data.

public:
    explicit WeatherProvider(QObject* object = nullptr);
    QList<QVariantMap> weatherData(); // Getter
    void initiateWeatherDataRequest();

public slots:
    void handleWeatherDataRequest(QNetworkReply *weatherDataResponse);

signals:
    void weatherDataChanged();

private:
    QList<QVariantMap> weather_data;
    QSettings* weatherSettings; // For api keys and latlong
};

#endif // WEATHERPROVIDER_H
