# Wall

Wall is an application you can run on a display on a wall.

Right now, Wall does a few things.

- Alarms
- Wallpaper (from Bing Image of the Day)
- Weather (from OpenWeatherMap API)
- Video/Media (from youtube-dl, only primarily works with YouTube).


Configuration is done with **config files** for this program. The program will print out your config file path when it runs, so **make sure to save your config files where it says the config file path is**.
Yes, this isn't very user friendly. That will be resolved later, hopefully.

## Alarms Configuration

Here is an example config (save as alarmSettings.conf). Make sure to follow the format for alarmTime.
Every time you change an alarm, make sure to restart the application. Also make sure to have preceding zeroes in times. (eg. 02)

 ```toml
[General]
alarmTime=06.09.2020 02:22 pm
alarmFile=/home/user/.config/tvrui/alarm_clock.wav
```


## Weather Configuration

Save this as weatherSettings.conf in the config file directory or else the weather widget won't load. Make sure to replace everything with your own values (apikey and latlong).
```toml
[General]
apikey=YOUR API KEY HERE
lat=1
long=1
units=imperial
```

## Video

You can just input a video into the input box in the program and press the button next to it to play the video.
However, there is an HTTP API for controlling videos. It runs on `http://localhost:8080`.


`/video/set?url=` Make sure to ... do it with the parameter in the URL, otherwise it doesn't work. I'll fix this eventually, hopefully

`/video/toggle` No params required. Just GET this and it will pause/play the video.

`/video/stop` No params required. Just GET this and it will stop the video and return to the main UI.


