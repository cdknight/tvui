# This Python file uses the following encoding: utf-8
import os
import sys

# YTDL's own python module is _so_ bad I'd rather just os.system

os.system(f"youtube-dl --quiet -f mp4 -o '{sys.argv[2]}/%(title)s.%(ext)s' " + sys.argv[1] + " > /dev/null") # Silent, pipe to devnull. Won't work on Windows. Don't support Windows.
os.system(f"youtube-dl --get-filename -f mp4 -o '{sys.argv[2]}/%(title)s.%(ext)s' " + sys.argv[1])
