#include "mediacontroller.h"
#include <QProcess>
#include <QStringList>
#include <QDebug>
#include <QObject>
#include <QDir>

MediaController::MediaController(QObject *parent) : QObject(parent)
{
    this->video_path = ""; // Initialize stuff
    this->loading = false;
}

void MediaController::setVideoUrl(QString url) {
   this->mediaDLProcess = new QProcess(); // This is a "subprocess" thing where we execute the python script to run youtube-dl to download the youtube videos.
   QObject::connect(this->mediaDLProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this, &MediaController::handleDownloadFinished); // Connect when the process (download) finishes (exits) to a function here.
   this->mediaDLProcess->start("python3", QStringList() << "scripts/dl.py" << url << QDir::tempPath() + "/tvui"); // Execute python3 with the ytdl script (and some args specifying the tmp dir & link)
   qDebug() << "(debug mediacontroller)" << "dl start";

   if (url != "") {
      // Only show the "loading prompt" if there's actually a video being loaded.
       this->loading = true; // Loading box's visibility in the UI is controlled by this
       emit loadingVideoChanged();
   }
}


void MediaController::handleDownloadFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "(debug mediacontroller)" << "exit code is" << exitCode;

    this->video_path = QString::fromStdString(this->mediaDLProcess->readAll().toStdString()).trimmed(); // Do some string parsing to get the filename of the downloaded youtube file
    this->loading = false; // We aren't loading anymore, emit those so the UI gets updated.
    emit videoFileChanged();
    emit loadingVideoChanged();
}

// The following two functions are just getters, so I don't need to explain it.
bool MediaController::loadingVideo() {
    return this->loading;
}

QString MediaController::videoFile() {
    return this->video_path;
}
