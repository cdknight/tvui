#ifndef MEDIACONTROLLER_H
#define MEDIACONTROLLER_H

#include <QObject>
#include <QProcess>

class MediaController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString videoFile READ videoFile WRITE setVideoUrl NOTIFY videoFileChanged) // Video file gets set to a URL but it becomes a file path instead which is returned by the ytdl output.
    Q_PROPERTY(bool loading READ loadingVideo NOTIFY loadingVideoChanged) // For showing that loading box.

public:
    explicit MediaController(QObject *parent = nullptr);
    QString videoFile(); // getter
    bool loadingVideo(); // getter
    void setVideoUrl(QString videoUrl); // setter (but doesn't actually set it to videoUrl as I said above)
signals:
    void videoFileChanged();
    void loadingVideoChanged();
public slots:
    void handleDownloadFinished(int exitCode, QProcess::ExitStatus exitStatus);
private:
    QString video_path;
    QProcess* mediaDLProcess;
    bool loading;
};

#endif // MEDIACONTROLLER_H
