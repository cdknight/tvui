#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "backgroundbackend/backgroundbackend.h"
#include "alarmcontroller/alarmcontroller.h"
#include "weatherprovider/weatherprovider.h"
#include "mediacontroller/mediacontroller.h"
#include "controlserver/controlserver.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    qmlRegisterType<BackgroundBackend>("cdknight.backgroundbackend", 1, 0, "BackgroundBackend"); // Import all the QML backend widgets (to control alarms, etc.)
    qmlRegisterType<WeatherProvider>("cdknight.weatherprovider", 1, 0, "WeatherProvider");
    qmlRegisterType<AlarmController>("cdknight.alarmcontroller", 1, 0, "AlarmController");
    qmlRegisterType<MediaController>("cdknight.mediacontroller", 1, 0, "MediaController");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml"))); // Load the interface from the main.qml file.

    ControlServer controlserver{engine}; // This is the web server. We instantiate it to... well run the web server.

    return app.exec();
}

