#ifndef CONTROLSERVER_H
#define CONTROLSERVER_H

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtHttpServer/QHttpServer>

class ControlServer
{
public:
    ControlServer(QQmlApplicationEngine& engine); // We need the QQmlApplicationEngine to change QML-based components (eg. the video components)
private:
    QQmlApplicationEngine& engine;
};

#endif // CONTROLSERVER_H
