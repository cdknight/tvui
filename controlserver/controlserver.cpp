#include "controlserver.h"
#include <QDebug>
#include <QtCore>
#include <QJsonObject>
#include <QtHttpServer/QtHttpServer>
#include <QMetaObject>
#include <QQmlProperty>
#include <QMediaPlayer>

ControlServer::ControlServer(QQmlApplicationEngine& engine): engine(engine)
{

    QHttpServer* server = new QHttpServer; // New HTTP server.
    server->route("/video/set", [this](const QHttpServerRequest &request) { // We handle three routes. This one is http://localhost:8080/video/set?url=<url> etc.
        QString playUrl = QUrlQuery(request.url()).queryItemValue("url"); // Get the url that we have to feed to the QML object.
        qDebug() << playUrl;
        qDebug() << this->engine.rootObjects().first()->findChild<QObject*>("videoWidget")->findChild<QObject*>("mediaController")->setProperty("videoFile", playUrl); // Find the mediaController QML object, and set the videoFile, which will trigger the file to download and play.
        return QJsonObject { // Return true. If something goes wrong... too bad.
            {"status", true}
        };
    });

    server->route("/video/toggle", [this]() {
        QObject* player(this->engine.rootObjects().first()->findChild<QObject*>("videoWidget")->findChild<QObject*>("videoPlayer"));
        int playingState = QQmlProperty::read(player, "playbackState").toInt(); // 0 -> stopped, 1 -> playing, 2 -> paused

        if (playingState == 1) { // Playing. Pause it.
            QMetaObject::invokeMethod(player, "pause");
        }
        else if (playingState == 2) { // Paused. Play it.
            QMetaObject::invokeMethod(player, "play");
        }

        return QJsonObject { // Return true. If something goes wrong... too bad.
            {"status", true}
        };
    });

    server->route("/video/stop", [this]() {
        qDebug() << this->engine.rootObjects().first()->findChild<QObject*>("videoWidget")->findChild<QObject*>("mediaController")->setProperty("videoFile", "");
        return QJsonObject { // Return true. If something goes wrong... too bad.
            {"status", true}
        };
    });

    server->listen(QHostAddress("127.0.0.1"), 8080); // Listen on port 8080. TODO let this be configurable with QSettings.

    qDebug() << "(debug controlserver) Control server running on" << QString("http://127.0.0.1:8080"); // Print some debug stuff.
}
